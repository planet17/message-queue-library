<?php

namespace Planet17\MessageQueueLibrary\Interfaces\Resolvers;

use Planet17\MessageQueueLibrary\Interfaces\Routes\RouteInterface;

/**
 * Interface RouteResolverInterface
 *
 * @package Planet17\MessageQueueLibrary\Interfaces\Resolvers
 */
interface RouteResolverInterface
{
    /**
     * Method resolve instance of route.
     *
     * Getter instance from internal pool.
     *
     * @param string $className
     *
     * @return mixed
     */
    public function resolve(string $className);

    /**
     * Method predicate for checking if exist instance inside of pool.
     *
     * @param $className
     *
     * @return mixed
     */
    public function hasRegister($className): bool;

    /**
     * Method add instance to pool.
     *
     * @param RouteInterface $route
     *
     * @return mixed
     */
    public function registerInstance(RouteInterface $route): void;
}
