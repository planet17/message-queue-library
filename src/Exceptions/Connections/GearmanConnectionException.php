<?php

namespace Planet17\MessageQueueLibrary\Exceptions\Connections;

use ErrorException;

/**
 * Class GearmanConnectionException
 *
 * @package Planet17\MessageQueueLibrary\Exceptions\Connections
 */
class GearmanConnectionException extends ErrorException
{
}
