<?php

namespace Planet17\MessageQueueLibrary\Exceptions\Connections;

use OutOfRangeException;
use Throwable;

/**
 * Class DriverProvidedNotFoundException
 *
 * @package Planet17\MessageQueueLibrary\Exceptions\Connections
 */
class DriverProvidedNotFoundException extends OutOfRangeException
{
    /**
     * DriverProvidedNotFoundException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct("Not found driver: '{$message}'", $code, $previous);
    }
}
