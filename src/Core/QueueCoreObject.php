<?php

namespace Planet17\MessageQueueLibrary\Core;

use Planet17\MessageQueueLibrary\Exceptions\Handlers\RouteClassNotDefinedException;
use Planet17\MessageQueueLibrary\Interfaces\Core\QueueCoreObjectInterface;
use Planet17\MessageQueueLibrary\Interfaces\Routes\RouteConnectableInterface;
use Planet17\MessageQueueLibrary\Interfaces\Routes\RouteInterface;

/**
 * Class QueueCoreObject
 *
 * @package Core
 */
abstract class QueueCoreObject implements QueueCoreObjectInterface
{
    /** @var null|string|RouteInterface */
    protected $routeClass = null;

    /** @var RouteConnectableInterface $route */
    private $route;

    /**
     * Getter Route instance.
     *
     * @return RouteConnectableInterface
     */
    public function getRoute(): RouteConnectableInterface
    {
        $this->ensureRouteInitialized();

        return $this->route;
    }

    /**
     * Create route whether it does not exist.
     *
     * @see HandlerInterface::__construct() Used that method.
     */
    private function ensureRouteInitialized(): void
    {
        if ($this->routeClass === null) {
            throw new RouteClassNotDefinedException;
        }

        $this->setRoute($this->routeClass::getInstance());
    }

    /**
     * Private setter Route instance.
     *
     * @param RouteInterface $route
     */
    private function setRoute(RouteInterface $route): void
    {
        $this->route = $route;
    }
}
