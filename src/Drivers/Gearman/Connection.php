<?php

namespace Planet17\MessageQueueLibrary\Drivers\Gearman;

use ErrorException;
use GearmanClient;
use GearmanJob;
use GearmanWorker;
use Planet17\MessageQueueLibrary\Exceptions\Connections\GearmanConnectionException;
use Planet17\MessageQueueLibrary\Exceptions\Handlers\UnexpectedHandlerReturnCodeException;
use Planet17\MessageQueueLibrary\Interfaces\Connections\ConnectionInterface;
use Planet17\MessageQueueLibrary\Interfaces\Handlers\HandlerInterface;
use Planet17\MessageQueueLibrary\Interfaces\Messages\MessageInterface;
use Planet17\MessageQueueLibrary\Interfaces\Messages\MessagePayloadInterface;
use Planet17\MessageQueueLibrary\Interfaces\Routes\RouteInterface;

/**
 * Class Connection
 *
 * @package Planet17\MessageQueueLibrary\Drivers\Gearman
 */
class Connection implements ConnectionInterface
{
    /** @var string $host */
    private $host;

    /** @var int $port */
    private $port;

    /** @var GearmanClient $connectionTransmitter */
    private $connectionTransmitter;

    /** @var GearmanWorker */
    private $connectionHandler;

    /** @var mixed */
    private $payload;

    /** @var HandlerInterface */
    private $handler;

    /** @var callable|mixed */
    private $callableHandle;

    /** @var string */
    private $routeAlias;

    /**
     * Connection constructor.
     *
     * @param string $host
     * @param int $port
     */
    public function __construct(string $host, int $port)
    {
        $this->host = $host;
        $this->port = $port;
    }

    /** @inheritdoc  */
    public function transmitMessage(RouteInterface $route, MessageInterface $message): void
    {
        $this->ensureConnectionTransmitting();
        $this->connectionTransmitter->doBackground($route->getAliasFull(), $message->getPayload());
    }

    /** @inheritdoc  */
    public function subscribeHandler(RouteInterface $route, HandlerInterface $handler, $callableHandle): void
    {
        $this->ensureConnectionHandler();
        $this->assign($route, $handler, $callableHandle);
        $this->initializeLoop();
    }

    /**
     * Register Handler function customized for Gearman.
     *
     * @param RouteInterface $route
     * @param HandlerInterface $handler
     * @param $callableHandle
     */
    private function assign(RouteInterface $route, HandlerInterface $handler, $callableHandle): void
    {
        $this->handler = $handler;
        $this->callableHandle = $callableHandle;
        $this->routeAlias = $route->getAliasFull();
        $this->registerHandle();
    }

    /**
     * Register Handler function customized for Gearman.
     */
    private function registerHandle(): void
    {
        $driver = $this;
        $this->connectionHandler->addFunction($this->routeAlias, static function(GearmanJob $job) use ($driver) {
            $payload = $driver->extractPayload($job);
            if ($driver->isTerminateSignal($payload)) {
                /* Manually mark message "completed" */
                $job->sendComplete(true);
                $driver->onKillSignal();
            }

            ($driver->callableHandle)($payload);
        });
    }

    /**
     * Method extracting payload and un-serialize it.
     *
     * @param GearmanJob $job
     *
     * @return mixed
     */
    private function extractPayload(GearmanJob $job)
    {
        return unserialize($job->workload(), $this->handler->getMessageAllowedClasses());
    }

    /**
     * Getter method.
     *
     * @return mixed
     */
    private function getPayload()
    {
        return $this->payload;
    }

    /**
     * Method predicate for check whether signal is "terminate".
     *
     * @param $payload
     *
     * @return bool
     */
    private function isTerminateSignal($payload): bool
    {
        return is_string($payload) && $payload === MessagePayloadInterface::PAYLOAD_TEXT_TERMINATE_HANDLER;
    }

    /**
     * Method init handling like a daemon.
     */
    private function initializeLoop(): void
    {
        while ($this->connectionHandler->work()) {
            if ($this->connectionHandler->returnCode() !== GEARMAN_SUCCESS) {
                throw new UnexpectedHandlerReturnCodeException($this->connectionHandler->returnCode());
            }
        }
    }

    /**
     * Whether connection for transmitting isn't exist will be creating.
     */
    private function ensureConnectionTransmitting(): void
    {
        if (!$this->connectionTransmitter) {
            set_error_handler([$this, 'exceptionOnWarning']);
            $this->connectionTransmitter = new GearmanClient;
            $this->connectionTransmitter->addServer($this->host, $this->port);
            restore_error_handler();
        }
    }

    /**
     * Whether connection for handling isn't exist will be creating.
     */
    private function ensureConnectionHandler(): void
    {
        if (!$this->connectionHandler) {
            set_error_handler([$this, 'exceptionOnWarning']);
            $this->connectionHandler = new GearmanWorker;
            $this->connectionHandler->addServer($this->host, $this->port);
            restore_error_handler();
        }
    }

    /**
     * Method for using exception instead of warning.
     *
     * @param $errNo
     * @param $errString
     * @param $errFile
     * @param $errLine
     * @param $errContext
     *
     * @return false
     * @throws ErrorException
     */
    public function exceptionOnWarning($errNo, $errString, $errFile, $errLine, $errContext): bool
    {
        throw new GearmanConnectionException($errString, 0, $errNo, $errFile, $errLine);
    }

    /**
     * Method for break infinite loop of daemon's handling.
     */
    private function onKillSignal(): void
    {
        die('Terminate daemon...');
    }
}
