<?php

namespace Planet17\MessageQueueLibrary\Interfaces\Routes;

/**
 * Interface RoutePackageInterface
 *
 * @package Planet17\MessageQueueLibrary\Interfaces\Routes
 */
interface RoutePackageInterface
{
    /**
     * Get common alias for package routes.
     *
     * @return string
     */
    public function getAliasTitle(): string;

    /**
     * Get compiled string of common alias and separator for concatenating it with package aliases.
     *
     * @return string
     */
    public function getAliasPrefix(): string;

    /**
     * Get all package classes provided by package.
     *
     * @return array|string[]
     */
    public function getPackagedRoutes(): array;
}
