<?php

namespace Planet17\MessageQueueLibrary\Interfaces\Connections;

/**
 * Interface MapDriversInterface
 *
 * @package Planet17\MessageQueueLibrary\Interfaces
 */
interface MapDriversInterface
{
    public const MAP = [
        MapDriversInterface::DRIVER_GEARMAN,
        MapDriversInterface::DRIVER_RABBIT_MQ,
    ];

    public const DRIVER_GEARMAN = 'Gearman';

    public const DRIVER_RABBIT_MQ = 'RabbitMQ';
}
