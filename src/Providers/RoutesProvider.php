<?php

namespace Planet17\MessageQueueLibrary\Providers;

use Planet17\MessageQueueLibrary\Interfaces\Providers\RoutesProviderInterface;
use Planet17\MessageQueueLibrary\Interfaces\Routes\RouteInterface;
use Planet17\MessageQueueLibrary\Interfaces\Routes\RoutePackageInterface;

/**
 * Class RoutesMapProvider
 *
 * @package Planet17\MessageQueueLibrary\Providers
 *
 * @see RoutesProviderInterface::provideRouteClasses() - override for provive some classes.
 * @see RoutesProviderInterface::providePackageClasses() - override for provive some classes.
 */
class RoutesProvider implements RoutesProviderInterface
{
    /** @var array $mapRouteAliases */
    private $mapRouteAliases = [];

    /** @var array $mapPackageAliases */
    private $mapPackageAliases = [];

    /**
     * RoutesMapProvider constructor.
     *
     * Calling method for check preset Routes by methods RoutesProviderInterface::provideRouteClasses() and
     * RoutesProviderInterface::providePackageClasses()
     *
     * @see RoutesProviderInterface::provideRouteClasses()
     * @see RoutesProviderInterface::providePackageClasses()
     */
    final public function __construct()
    {
        $this->analyzeRoutes();
        $this->analyzePackages();
    }

    /** @inheritdoc */
    public function provideRouteClasses(): array
    {
        return [];
    }

    /** @inheritdoc  */
    public function providePackageClasses(): array
    {
        return [];
    }

    /**
     * Return array with key aliases and value RouteInterface.
     *
     * @return array
     */
    public function getMapped(): array
    {
        return $this->mapRouteAliases;
    }

    /**
     * Getter method is return Route.
     *
     * @param string $aliasName
     *
     * @return mixed
     */
    public function getRouteByAlias(string $aliasName)
    {
        if (!array_key_exists($aliasName, $this->mapRouteAliases)) {
            throw new \RuntimeException($aliasName);
        }

        return $this->mapRouteAliases[$aliasName];
    }

    /**
     * Method for encapsulate expression in one row.
     *
     * @param $class
     *
     * @return mixed
     */
    private function forgeInstanceByClass($class)
    {
        return new $class;
    }

    /**
     * Method for call registration all non-packaged routes provided by provideRouteClasses().
     *
     * @return void
     *
     * @see RoutesProvider::provideRouteClasses()
     */
    private function analyzeRoutes(): void
    {
        $this->addRoutes(...$this->provideRouteClasses());
    }

    /**
     * Method for call registration all packages provided by providePackageClasses().
     *
     * @return void
     *
     * @see RoutesProvider::providePackageClasses()
     */
    private function analyzePackages(): void
    {
        /** @var RoutePackageInterface $providerPackageClasses */
        foreach ($this->providePackageClasses() as $providerPackageClasses) {
            $this->registerPackage($this->forgeInstanceByClass($providerPackageClasses));
        }
    }

    /**
     * Method for validate and register provided routes.
     *
     * @param RouteInterface|string ...$routeClasses
     */
    private function addRoutes(...$routeClasses): void
    {
        foreach ($routeClasses as $routeClass) {
            $this->registerRoute($routeClass::getInstance());
        }
    }

    /**
     * Method for registering Route instance.
     *
     * @param RouteInterface $route
     */
    private function registerRoute(RouteInterface $route): void
    {
        $this->mapRouteAliases[$route->getAliasFull()] = $route;
    }

    /**
     * @param RoutePackageInterface $package
     */
    private function registerPackage(RoutePackageInterface $package): void
    {
        $this->mapPackageAliases[$package->getAliasTitle()] = $package;
        $this->addRoutes(...$package->getPackagedRoutes());
    }
}
