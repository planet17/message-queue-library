# Message Queue Library `(version 0.9.3.1)` #

### Example ###

```php

class ExampleRoute extends \Planet17\MessageQueueLibrary\Routes\ConnectableRoute
{
    /** @var string $connectionName */
    protected $connectionName = 'example';

    /** @inheritdoc  */
    public function getAliasShort(): string
    {
        return 'example';
    }
}

class ExampleProvider extends \Planet17\MessageQueueLibrary\Providers\RoutesProvider
{
    public function provideRouteClasses(): array
    {
        return [
            ExampleRoute::class
        ];
    }
}

class ExampleMessage extends \Planet17\MessageQueueLibrary\Messages\BaseMessage
{
    protected $routeClass = ExampleRoute::class;
}

class ExampleHandler extends \Planet17\MessageQueueLibrary\Handlers\BaseHandler
{

    /** @var string $routeClass */
    protected $routeClass = ExampleRoute::class;

    /** @inheritdoc  */
    public function handle(): void
    {
        sleep(1);
        echo 'Completed' . PHP_EOL;
    }
}

class Manager extends \Planet17\MessageQueueLibrary\Connections\ConnectionManagerDefault
{
    public function getRoutesProvider(): \Planet17\MessageQueueLibrary\Interfaces\Providers\RoutesProviderInterface
    {
        return new ExampleProvider;
    }
}

Manager::getInstance()
    ->addConnection(
        [
            'driver' => 'Gearman',
            'host' => 'localhost',
            'port' => 4730,
        ],
        'example'
    )
    ->bootRoutes();

/* Dispatch 10 messages to Queue */
for ($i = 10; $i--; ) {
    (new ExampleMessage)->dispatch();
}

/* Start worker/daemon loop. */
(new ExampleHandler)->initialize();
```
