<?php

namespace Planet17\MessageQueueLibrary\Interfaces\Connections;

use Planet17\MessageQueueLibrary\Interfaces\Providers\RoutesProviderInterface;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\ConnectionResolverInterface;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\RouteResolverInterface;

/**
 * Interface ConnectionManagerInterface
 *
 * @package Planet17\MessageQueueLibrary\Interfaces\Connections
 */
interface ManagerInterface
{
    /** @const DEFAULT_CONNECTION_NAME string */
    public const DEFAULT_CONNECTION_NAME = 'default';

    /**
     * Method for public access for instance __construct.
     *
     * @return $this
     */
    public static function getInstance(): ManagerInterface;

    /**
     * Method set resolver to Routes (connections and instances for getting it from one pool).
     *
     * Important method for set-up resolver to Routes and Connections before using.
     */
    public function bootRoutes(): void;

    /**
     * Method add connection configuration
     *
     * @param array $config
     * @param string $name
     *
     * @return ManagerInterface
     */
    public function addConnection(array $config, $name = self::DEFAULT_CONNECTION_NAME): ManagerInterface;

    /**
     * Implement method for getter new resolver's instance of routes.
     *
     * @return RouteResolverInterface
     */
    public function getResolverRouteInstance(): RouteResolverInterface;

    /**
     * Implement method for getter new resolver's instance of connections.
     *
     * @return ConnectionResolverInterface
     */
    public function getResolverConnection(): ConnectionResolverInterface;

    /**
     * Getter for configuration set by provided connection name.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function getConfiguration($name = ManagerInterface::DEFAULT_CONNECTION_NAME);

    /**
     * Implement method with providing new instance of routes provider.
     *
     * @return RoutesProviderInterface
     */
    public function getRoutesProvider(): RoutesProviderInterface;
}
