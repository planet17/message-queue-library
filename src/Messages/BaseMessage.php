<?php

namespace Planet17\MessageQueueLibrary\Messages;

use Planet17\MessageQueueLibrary\Core\QueueCoreObject;
use Planet17\MessageQueueLibrary\Interfaces\Messages\MessageInterface;

/**
 * Class BaseHandler
 *
 * @package Planet17\MessageQueueLibrary\Handlers
 */
abstract class BaseMessage extends QueueCoreObject implements MessageInterface
{
    /** @var mixed */
    private $payload;

    /** @inheritdoc */
    public function __construct($payload = null)
    {
        if ($payload) {
            $this->setPayload($payload);
        }
    }

    /** @inheritdoc  */
    public function dispatch($route = null): void
    {
        $route = $route ?? $this->getRoute();
        $route->transmitMessage($this);
    }

    /** @inheritdoc  */
    public function setPayload($payload): MessageInterface
    {
        $this->payload = $payload;

        return $this;
    }

    /** @inheritdoc  */
    public function getPayload(): string
    {
        return serialize($this->payload);
    }
}
