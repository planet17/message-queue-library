<?php

namespace Planet17\MessageQueueLibrary\Exceptions\Connections;

use InvalidArgumentException;

/**
 * Class DriverProvidedInvalidException
 *
 * @package Planet17\MessageQueueLibrary\Exceptions\Connections
 */
class DriverProvidedInvalidException extends InvalidArgumentException
{
}
