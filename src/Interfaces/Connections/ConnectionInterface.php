<?php

namespace Planet17\MessageQueueLibrary\Interfaces\Connections;

use Planet17\MessageQueueLibrary\Interfaces\Handlers\HandlerInterface;
use Planet17\MessageQueueLibrary\Interfaces\Messages\MessageInterface;
use Planet17\MessageQueueLibrary\Interfaces\Routes\RouteInterface;

/**
 * Interface ConnectionInterface
 *
 * @package Planet17\MessageQueueLibrary\Interfaces
 */
interface ConnectionInterface
{
    /**
     * Call methods providing message via `queue`.
     *
     * @param RouteInterface $route
     * @param MessageInterface $message
     */
    public function transmitMessage(RouteInterface $route, MessageInterface $message): void;

    /**
     * Call methods for starting subscribing and waiting for receiving message.
     *
     * @param RouteInterface $route
     * @param HandlerInterface $handler
     * @param $callableHandle
     */
    public function subscribeHandler(RouteInterface $route, HandlerInterface $handler, $callableHandle): void;
}
