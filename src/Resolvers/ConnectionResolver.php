<?php

namespace Planet17\MessageQueueLibrary\Resolvers;

use Planet17\MessageQueueLibrary\Connections\ConnectionManagerBase;
use Planet17\MessageQueueLibrary\Connections\ConnectionFactory;
use Planet17\MessageQueueLibrary\Interfaces\Connections\ConnectionInterface;
use Planet17\MessageQueueLibrary\Interfaces\Connections\ManagerInterface;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\ConnectionResolverInterface;

/**
 * Class ConnectionResolver
 *
 * @package Planet17\MessageQueueLibrary\Resolvers
 */
class ConnectionResolver implements ConnectionResolverInterface
{
    /** @var ConnectionFactory $factory */
    private $factory;

    /** @var ConnectionManagerBase $manager */
    private $manager;

    /** @var array $created Memory storage of created connection. */
    private $created = [];

    /** @inheritdoc  */
    public function __construct(ConnectionFactory $connectionFactory, ManagerInterface $manager)
    {
        $this->factory = $connectionFactory;
        $this->manager = $manager;
    }

    /** @inheritdoc */
    public function resolve(string $connectionName): ConnectionInterface
    {
        if (!array_key_exists($connectionName, $this->created)) {
            $configuration = $this->manager->getConfiguration($connectionName);
            $this->created[$connectionName] = $this->factory->factoryConnection($configuration);
        }

        return $this->created[$connectionName];
    }
}
