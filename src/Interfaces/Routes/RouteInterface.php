<?php

namespace Planet17\MessageQueueLibrary\Interfaces\Routes;

use Planet17\MessageQueueLibrary\Interfaces\Resolvers\RouteResolverInterface;

/**
 * Interface RouteInterfaces
 *
 * @package Planet17\MessageQueueLibrary\Interfaces
 */
interface RouteInterface
{
    /**
     * Return instance of route.
     *
     * Whether is exist it getting from property with resolver.
     *
     * Whether isn't exist it will be created and register to property with resolver.
     *
     * @return RouteInterface
     */
    public static function getInstance(): RouteInterface;

    /**
     * Set instance resolver for creating only one instance of route.
     *
     * @param RouteResolverInterface $resolver
     */
    public static function setInstanceResolver(RouteResolverInterface $resolver): void;

    /**
     * Method must return a unique alias of itself.
     *
     * @return string
     */
    public function getAliasShort(): string;

    /**
     * Method must return compiling full alias of package whether it exists and a unique alias of itself.
     *
     * @return string
     */
    public function getAliasFull(): string;

    /**
     * Method assign parent package of route.
     *
     * It will apply on `full alias`.
     *
     * @param RoutePackageInterface $package
     */
    public function assignToPackage(RoutePackageInterface $package): void;
}
