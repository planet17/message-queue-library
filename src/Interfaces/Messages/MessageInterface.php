<?php

namespace Planet17\MessageQueueLibrary\Interfaces\Messages;

use Planet17\MessageQueueLibrary\Interfaces\Core\QueueCoreObjectInterface;
use Planet17\MessageQueueLibrary\Interfaces\Routes\RouteInterface;

/**
 * Interface MessageInterface
 *
 * @package Planet17\MessageQueueLibrary\Interfaces
 */
interface MessageInterface extends QueueCoreObjectInterface
{
    /**
     * MessageInterface constructor.
     *
     * @param null $payload
     */
    public function __construct($payload = null);

    /**
     * Method return serialized payload of message.
     *
     * @return string
     */
    public function getPayload(): string;

    /**
     * Method call dispatch message to queue.
     *
     * @param mixed|RouteInterface|string|null $route
     *
     * @return void
     */
    public function dispatch($route = null): void;
}
