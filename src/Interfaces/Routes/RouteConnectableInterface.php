<?php

namespace Planet17\MessageQueueLibrary\Interfaces\Routes;

use Planet17\MessageQueueLibrary\Interfaces\Connections\ConnectionInterface;
use Planet17\MessageQueueLibrary\Interfaces\Handlers\HandlerInterface;
use Planet17\MessageQueueLibrary\Interfaces\Messages\MessageInterface;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\ConnectionResolverInterface;

/**
 * Interface RouteConnectableInterface
 *
 * @package Planet17\MessageQueueLibrary\Interfaces\Routes
 */
interface RouteConnectableInterface extends RouteInterface
{
    /**
     * Method for set up connection resolver for creating connection by preset from Route class.
     *
     * @param $resolver
     */
    public static function setConnectionResolver(ConnectionResolverInterface $resolver): void;

    /**
     * Method return connection.
     *
     * @return ConnectionInterface
     */
    public function getConnection(): ConnectionInterface;

    /**
     * Method for call transmitting message via ConnectionInterface::class instance.
     *
     * @param MessageInterface $message
     */
    public function transmitMessage(MessageInterface $message): void;

    /**
     * Method for call subscribing message via ConnectionInterface::class instance.
     * @param HandlerInterface $handler
     * @param $callableHandle
     */
    public function subscribeHandler(HandlerInterface $handler, $callableHandle): void;
}
