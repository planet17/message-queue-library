<?php

namespace Planet17\MessageQueueLibrary\Routes;

use Planet17\MessageQueueLibrary\Interfaces\Connections\ConnectionInterface;
use Planet17\MessageQueueLibrary\Interfaces\Handlers\HandlerInterface;
use Planet17\MessageQueueLibrary\Interfaces\Messages\MessageInterface;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\ConnectionResolverInterface;
use Planet17\MessageQueueLibrary\Interfaces\Routes\RouteConnectableInterface;
use Serializable;

/**
 * Class ConnectableRoute
 *
 * @package Planet17\MessageQueueLibrary\Routes
 */
abstract class ConnectableRoute extends BaseRoute implements RouteConnectableInterface, Serializable
{
    /** @var ConnectionResolverInterface $resolverConnection */
    protected static $resolverConnection;

    /** @var ConnectionInterface $connection */
    private $connection;

    /** @var string $connectionName Name of connection group at the Manager's configuration. */
    protected $connectionName = 'default';

    /** @inheritdoc  */
    public static function setConnectionResolver(ConnectionResolverInterface $resolver): void
    {
        static::$resolverConnection = $resolver;
    }

    /** @inheritdoc  */
    public function getConnection(): ConnectionInterface
    {
        $this->ensureConnectionResolved();

        return $this->connection;
    }

    /** Initialize connection whether it does not exist. */
    private function ensureConnectionResolved(): void
    {
        if (!$this->connection) {
            $this->connection = static::$resolverConnection->resolve($this->connectionName);
        }
    }

    /** @inheritdoc  */
    public function transmitMessage(MessageInterface $message): void
    {
        $this->getConnection()->transmitMessage($this, $message);
    }

    /** @inheritdoc  */
    public function subscribeHandler(HandlerInterface $handler, $callableHandle): void
    {
        $this->getConnection()->subscribeHandler($this, $handler, $callableHandle);
    }

    /** @inheritDoc */
    public function serialize(): ?string
    {
        return serialize($this->connectionName);
    }

    /** @inheritDoc */
    public function unserialize($serialized): void
    {
        $this->connectionName = unserialize($serialized, ['allowed_classes' => false]);
    }
}
