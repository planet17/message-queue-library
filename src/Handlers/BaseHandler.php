<?php

namespace Planet17\MessageQueueLibrary\Handlers;

use Planet17\MessageQueueLibrary\Core\QueueCoreObject;
use Planet17\MessageQueueLibrary\Interfaces\Handlers\HandlerInterface;

/**
 * Class BaseHandler
 *
 * @package Planet17\MessageQueueLibrary\Handlers
 */
abstract class BaseHandler extends QueueCoreObject implements HandlerInterface
{
    /** @inheritdoc */
    public function initialize(): void
    {
        $this->getRoute()->subscribeHandler($this, [$this, 'handle']);
    }

    /** @inheritdoc */
    public function handle($payload): void
    {
    }

    /** @inheritdoc  */
    public function getMessageAllowedClasses(): array
    {
        return [false];
    }
}
