<?php

namespace Planet17\MessageQueueLibrary\Interfaces\Messages;

/**
 * Class MessagePayloadInterface
 *
 * @package Planet17\MessageQueueLibrary\Interfaces\Messages
 */
interface MessagePayloadInterface
{
    /** @const PAYLOAD_TEXT_TERMINATE_HANDLER string */
    public const PAYLOAD_TEXT_TERMINATE_HANDLER = 'terminate';
}
