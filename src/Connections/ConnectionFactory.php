<?php

namespace Planet17\MessageQueueLibrary\Connections;

use Planet17\MessageQueueLibrary\Drivers\Gearman\Connection as Gearman;
use Planet17\MessageQueueLibrary\Exceptions\Connections\DriverProvidedInvalidException;
use Planet17\MessageQueueLibrary\Exceptions\Connections\DriverProvidedNotFoundException;
use Planet17\MessageQueueLibrary\Interfaces\Connections\ConnectionInterface;
use Planet17\MessageQueueLibrary\Interfaces\Connections\MapDriversInterface;

/**
 * Class Factory
 *
 * @package Planet17\MessageQueueLibrary\Connections
 */
class ConnectionFactory
{
    /** @const DEFAULT_HOST_GEARMAN string */
    private const DEFAULT_HOST_GEARMAN = 'localhost';

    /** @const DEFAULT_PORT_GEARMAN int */
    private const DEFAULT_PORT_GEARMAN = 4730;

    /**
     * Method for create connection instance with provided configuration.
     *
     * @param array $configuration
     *
     * @return ConnectionInterface
     */
    public function factoryConnection(array $configuration): ConnectionInterface
    {
        if (!array_key_exists('driver', $configuration) || !is_string($configuration['driver'])) {
            throw new DriverProvidedInvalidException(serialize($configuration));
        }

        switch ($configuration['driver']) {
            case MapDriversInterface::DRIVER_GEARMAN:
                return new Gearman(
                    $configuration['host'] ?? static::DEFAULT_HOST_GEARMAN,
                    $configuration['port'] ?? static::DEFAULT_PORT_GEARMAN
                );
            case MapDriversInterface::DRIVER_RABBIT_MQ:
            default:
                throw new DriverProvidedNotFoundException($configuration['driver']);
        }
    }
}
