<?php

namespace Planet17\MessageQueueLibrary\Exceptions\Routes;

use RuntimeException;
use Throwable;

/**
 * Class UndefinedResolverRoutesException
 *
 * @package Planet17\MessageQueueLibrary\Exceptions\Routes
 */
class UndefinedResolverRoutesException extends RuntimeException
{
    /**
     * UndefinedResolverRoutesException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Resolver must been preset to Route.', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
