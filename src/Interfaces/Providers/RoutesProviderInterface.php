<?php

namespace Planet17\MessageQueueLibrary\Interfaces\Providers;

/**
 * Interface RoutesProviderInterface
 *
 * @package Planet17\MessageQueueLibrary\Interfaces\Providers
 */
interface RoutesProviderInterface extends BaseProviderInterface
{
    /**
     * Override it with your set.
     *
     * Implement set of Routes for including in map.
     *
     * @return string[]
     */
    public function provideRouteClasses(): array;

    /**
     * Override it with your set.
     *
     * Implement set of Packages with Routes for including in map.
     *
     * @return string[]
     */
    public function providePackageClasses(): array;
}
