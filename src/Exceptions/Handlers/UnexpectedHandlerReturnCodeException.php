<?php

namespace Planet17\MessageQueueLibrary\Exceptions\Handlers;

use RuntimeException;
use Throwable;

/**
 * Class UnexpectedHandlerReturnCodeException
 *
 * @package Planet17\MessageQueueLibrary\Exceptions\Handlers
 */
class UnexpectedHandlerReturnCodeException extends RuntimeException
{
    /**
     * UnexpectedHandlerReturnCodeException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        parent::__construct('Return Code: ' . $message, $code, $previous);
    }
}
