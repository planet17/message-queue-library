<?php

namespace Planet17\MessageQueueLibrary\Routes;

use Planet17\MessageQueueLibrary\Interfaces\Routes\RoutePackageInterface;

/**
 * Class RoutePackage
 *
 * @package Planet17\MessageQueueLibrary\Routes
 */
abstract class RoutePackage implements RoutePackageInterface
{
    private const DEFAULT_ALIAS_SEPARATOR = '-';

    /** @inheritdoc  */
    abstract public function getAliasTitle(): string;

    /** @inheritdoc  */
    abstract public function getPackagedRoutes(): array;

    /** @inheritdoc */
    public function getAliasPrefix(): string
    {
        return $this->getAliasTitle() . self::DEFAULT_ALIAS_SEPARATOR;
    }
}
