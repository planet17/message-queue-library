<?php

namespace Planet17\MessageQueueLibrary\Connections;

use Planet17\MessageQueueLibrary\Exceptions\Connections\InvalidConnectionNameException;
use Planet17\MessageQueueLibrary\Interfaces\Connections\ManagerInterface;
use Planet17\MessageQueueLibrary\Interfaces\Providers\RoutesProviderInterface;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\ConnectionResolverInterface;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\RouteResolverInterface;
use Planet17\MessageQueueLibrary\Routes\ConnectableRoute;
use RuntimeException;

/**
 * Class Manager
 *
 * @package Planet17\MessageQueueLibrary\Connections
 */
abstract class ConnectionManagerBase implements ManagerInterface
{
    /** @var ManagerInterface|null $instance Make a manager like a singleton. */
    protected static $instance = null;

    /** @var array $connections Connection configuration. */
    private $connections = [];

    /**
     * Manager constructor.
     *
     * Deny for calling it from outside.
     */
    final protected function __construct(){}

    /** @inheritdoc  */
    public function addConnection(array $config, $name = self::DEFAULT_CONNECTION_NAME): ManagerInterface
    {
        $this->connections[$name] = $config;

        return $this;
    }

    /** @inheritdoc  */
    public function bootRoutes(): void
    {
        ConnectableRoute::setInstanceResolver($this->getResolverRouteInstance());
        ConnectableRoute::setConnectionResolver($this->getResolverConnection());
        $this->getRoutesProvider();
    }

    /** @inheritdoc */
    abstract public function getResolverRouteInstance(): RouteResolverInterface;

    /** @inheritdoc  */
    abstract public function getResolverConnection(): ConnectionResolverInterface;

    /** @inheritdoc  */
    abstract public function getRoutesProvider(): RoutesProviderInterface;

    /** @inheritdoc */
    public function getConfiguration($name = self::DEFAULT_CONNECTION_NAME)
    {
        if (!array_key_exists($name, $this->connections)) {
            throw new InvalidConnectionNameException($name);
        }

        return $this->connections[$name];
    }

    /** @inheritdoc */
    public static function getInstance(): ManagerInterface
    {
        if (static::$instance === null) {
            static::$instance = new static;
        }

        return static::$instance;
    }

    /**
     * Deny by singleton rules.
     */
    final protected function __clone() { }

    /**
     * Deny by singleton rules.
     *
     * @throws RuntimeException
     */
    final protected function __wakeup()
    {
        throw new RuntimeException('Cannot un-serialize a singleton.');
    }
}
