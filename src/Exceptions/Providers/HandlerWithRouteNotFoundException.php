<?php

namespace Planet17\MessageQueueLibrary\Exceptions\Providers;

use Throwable;

/**
 * Class HandlerWithRouteNotFound
 *
 * @package Planet17\MessageQueueLibrary\Exceptions\Providers
 */
class HandlerWithRouteNotFoundException extends WrongProviderSettingException
{
    /**
     * HandlerWithRouteNotFoundException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = '', $code = 0, Throwable $previous = null)
    {
        $message = 'Handler with Route class ' . $message . ' not found.';
        parent::__construct($message, $code, $previous);
    }
}
