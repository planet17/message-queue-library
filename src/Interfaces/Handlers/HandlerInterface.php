<?php

namespace Planet17\MessageQueueLibrary\Interfaces\Handlers;

use Planet17\MessageQueueLibrary\Interfaces\Core\QueueCoreObjectInterface;
use Planet17\MessageQueueLibrary\Interfaces\Messages\MessageInterface;

/**
 * Interface HandlerInterface
 *
 * @package Planet17\MessageQueueLibrary\Interfaces\Handlers
 */
interface HandlerInterface extends QueueCoreObjectInterface
{
    /**
     * Method start waiting incoming message (subscribing) and process them.
     *
     * Start daemon's work.
     */
    public function initialize(): void;

    /**
     * Method must implement handle of message
     *
     * @param mixed|string|MessageInterface $payload
     */
    public function handle($payload): void;

    /**
     * Method return allowed classes for un-serialize message.
     *
     * By default it return any classes.
     *
     * You can override it for check whether provided message classes is valid.
     *
     * @return array
     */
    public function getMessageAllowedClasses(): array;
}
