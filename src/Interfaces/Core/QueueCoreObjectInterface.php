<?php

namespace Planet17\MessageQueueLibrary\Interfaces\Core;

use Planet17\MessageQueueLibrary\Interfaces\Routes\RouteConnectableInterface;

/**
 * Class QueueCoreObjectInterface
 *
 * @package Interfaces\Core
 */
interface QueueCoreObjectInterface
{
    /**
     * Method route on what subscribed handler.
     *
     * @return RouteConnectableInterface
     */
    public function getRoute(): RouteConnectableInterface;
}
