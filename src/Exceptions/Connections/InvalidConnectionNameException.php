<?php

namespace Planet17\MessageQueueLibrary\Exceptions\Connections;

use OutOfRangeException;

/**
 * Class InvalidConnectionName
 *
 * @package Planet17\MessageQueueLibrary\Exceptions\Connections
 */
class InvalidConnectionNameException extends OutOfRangeException
{
}
