<?php

namespace Planet17\MessageQueueLibrary\Exceptions\Routes;

use OutOfRangeException;

/**
 * Class UnregisterRouteClassException
 *
 * @package Planet17\MessageQueueLibrary\Exceptions\Routes
 */
class UnregisterRouteClassException extends OutOfRangeException
{
}
