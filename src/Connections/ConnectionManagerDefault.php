<?php

namespace Planet17\MessageQueueLibrary\Connections;

use Planet17\MessageQueueLibrary\Interfaces\Providers\RoutesProviderInterface;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\ConnectionResolverInterface;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\RouteResolverInterface;
use Planet17\MessageQueueLibrary\Providers\RoutesProvider;
use Planet17\MessageQueueLibrary\Resolvers\ConnectionResolver;
use Planet17\MessageQueueLibrary\Resolvers\RouteInstanceResolver;

/**
 * Class Manager
 *
 * @package Planet17\MessageQueueLibrary\Connections
 */
class ConnectionManagerDefault extends ConnectionManagerBase
{
    /**
     * Provide default resolver. Override it whether is need.
     * @inheritdoc
     */
    public function getResolverRouteInstance(): RouteResolverInterface
    {
        return new RouteInstanceResolver;
    }

    /**
     * @inheritdoc
     * Provide default resolver. Override it whether is need.
     */
    public function getResolverConnection(): ConnectionResolverInterface
    {
        return new ConnectionResolver(new ConnectionFactory, $this);
    }

    /**
     * @inheritdoc
     * Provide default provider. Override it whether is need.
     */
    public function getRoutesProvider(): RoutesProviderInterface
    {
        return new RoutesProvider;
    }
}
