<?php

namespace Planet17\MessageQueueLibrary\Exceptions\Providers;

use Throwable;

/**
 * Class InconsistencyProvidersException
 *
 * @package Planet17\MessageQueueLibrary\Exceptions\Providers
 */
class InconsistencyProvidersException extends WrongProviderSettingException
{
    /** @const DEFAULT_MESSAGE string  */
    private const DEFAULT_MESSAGE = 'Providers must have equal number of elements.';

    /**
     * InconsistencyProvidersException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = self::DEFAULT_MESSAGE, $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
