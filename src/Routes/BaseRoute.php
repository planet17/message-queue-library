<?php

namespace Planet17\MessageQueueLibrary\Routes;

use Planet17\MessageQueueLibrary\Exceptions\Routes\UndefinedResolverRoutesException;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\RouteResolverInterface;
use Planet17\MessageQueueLibrary\Interfaces\Routes\RouteInterface;
use Planet17\MessageQueueLibrary\Interfaces\Routes\RoutePackageInterface;

/**
 * Class BaseRoute
 *
 * @package Planet17\MessageQueueLibrary\Routes
 */
abstract class BaseRoute implements RouteInterface
{
    /** @var RouteResolverInterface|null */
    protected static $resolverInstance = null;

    /** @var RoutePackageInterface|null $package */
    private $package = null;

    /**
     * BaseRoute constructor.
     *
     * Deny use __constructor from outside.
     */
    final protected function __construct()
    {
    }

    public static function setInstanceResolver(RouteResolverInterface $resolver): void
    {
        static::$resolverInstance = $resolver;
    }

    /** @inheritdoc  */
    public static function getInstance(): RouteInterface
    {
        if (static::$resolverInstance === null) {
            throw new UndefinedResolverRoutesException;
        }

        if (!static::$resolverInstance->hasRegister(static::class)) {
            static::$resolverInstance->registerInstance(new static);
        }

        return static::$resolverInstance->resolve(static::class);
    }

    /** @inheritdoc  */
    abstract public function getAliasShort(): string;

    /** @inheritdoc  */
    public function getAliasFull(): string
    {
        if ($this->package === null) {
            return $this->getAliasShort();
        }

        return $this->package->getAliasPrefix() . $this->getAliasShort();
    }

    /** @inheritdoc  */
    public function assignToPackage(RoutePackageInterface $package):void
    {
        $this->package = $package;
    }
}
