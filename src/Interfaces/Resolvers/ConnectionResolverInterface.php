<?php

namespace Planet17\MessageQueueLibrary\Interfaces\Resolvers;

use Planet17\MessageQueueLibrary\Connections\ConnectionFactory;
use Planet17\MessageQueueLibrary\Interfaces\Connections\ConnectionInterface;
use Planet17\MessageQueueLibrary\Interfaces\Connections\ManagerInterface;

/**
 * Interface ConnectionResolverInterface
 *
 * @package Planet17\MessageQueueLibrary\Interfaces\Resolvers
 */
interface ConnectionResolverInterface
{
    /**
     * Resolver constructor.
     *
     * @param ConnectionFactory $connectionFactory
     * @param ManagerInterface $manager
     */
    public function __construct(ConnectionFactory $connectionFactory, ManagerInterface $manager);

    /**
     * @param string $connectionName
     *
     * @return ConnectionInterface
     */
    public function resolve(string $connectionName): ConnectionInterface;
}
