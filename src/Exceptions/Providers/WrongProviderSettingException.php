<?php

namespace Planet17\MessageQueueLibrary\Exceptions\Providers;

use LogicException;

/**
 * Class WrongProviderSettingException
 *
 * @package Planet17\MessageQueueLibrary\Exceptions\Providers
 */
class WrongProviderSettingException extends LogicException
{
}
