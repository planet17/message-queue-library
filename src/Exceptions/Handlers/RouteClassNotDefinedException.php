<?php

namespace Planet17\MessageQueueLibrary\Exceptions\Handlers;

use RuntimeException;
use Throwable;

/**
 * Class RouteClassNotDefinedException
 *
 * @package Planet17\MessageQueueLibrary\Exceptions\Handlers
 */
class RouteClassNotDefinedException extends RuntimeException
{
    /**
     * RouteClassNotDefinedException constructor.
     *
     * @param string $message
     * @param int $code
     * @param Throwable|null $previous
     */
    public function __construct($message = 'Route class not defined for Handler.', $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
