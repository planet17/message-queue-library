<?php

namespace Planet17\MessageQueueLibrary\Interfaces\Providers;

/**
 * Interface ProviderInterface
 *
 * @package Planet17\MessageQueueLibrary\Interfaces\Providers
 */
interface BaseProviderInterface
{
    /**
     * Return array with mapped instances.
     *
     * @return array
     */
    public function getMapped(): array;
}
