<?php

namespace Planet17\MessageQueueLibrary\Resolvers;

use Planet17\MessageQueueLibrary\Exceptions\Routes\UnregisterRouteClassException;
use Planet17\MessageQueueLibrary\Interfaces\Resolvers\RouteResolverInterface;
use Planet17\MessageQueueLibrary\Interfaces\Routes\RouteInterface;

/**
 * Class RouteResolver
 *
 * @package Planet17\MessageQueueLibrary\Resolvers
 */
class RouteInstanceResolver implements RouteResolverInterface
{
    /** @var array|RouteInterface[] Array of routes with key like their class name. */
    private $pool = [];

    /** @inheritdoc  */
    public function resolve(string $className)
    {
        if (!$this->hasRegister($className)) {
            throw new UnregisterRouteClassException($className);
        }

        return $this->pool[$className];
    }

    /** @inheritdoc  */
    public function hasRegister($className): bool
    {
        return array_key_exists($className, $this->pool);
    }

    /** @inheritdoc  */
    public function registerInstance(RouteInterface $route): void
    {
        $this->pool[get_class($route)] = $route;
    }
}
